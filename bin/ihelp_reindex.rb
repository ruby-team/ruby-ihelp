#!/usr/bin/ruby

require 'rubygems'
require 'ihelp'
begin
  require 'ferret'
rescue Exception
  STDERR.puts "Unable to load Ferret, can't update full text index."
  STDERR.puts "Try `gem install ferret' if you don't have already."
  exit 1
end

IHelp::IHelpIndex.reindex_when_needed = true
idx = IHelp::IHelpIndex.new
if idx.path == IHelp::IHelpIndex::GLOBAL_INDEX_PATH
 begin
  STDERR.puts
  STDERR.puts 'Making global index at '+idx.path+' readable'
  Dir[File.join(idx.path,"*")].each{|n| File.chmod 0644, n }
 end
end
